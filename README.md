# Responsive slideshow with embedded pages #
---
## Features ##

* The content and the appearance settings in HTML/CSS only (JavaScript experience is not necessary). All logical structure is defined by DOM elements (div, section, lists) 
* Responsive (supports both a wide and a narrow screen)
* Auto-adjustment for images inside slides (with aspect ratio)
* Friendly for search engines (due to content which is defined statically inside HTML)
* Supports a deep linking by unique ID of images (authomatic or user-defined link)
* Supports HTML5 and CSS3 (for transition effects)
* Own grid system (independent from Bootstrap, etc)

## Dependencies ##
* jQuery
* a custom version of Lightslider (with a several updates, embedded into the code)
* Font Awesome (optional, for glyph icons, you can choose any other glyphs for collapsible elements and for slideshow buttons)
* Google Fonts (optional, you can choose any other fonts)

## Application note ##

Here is a boilerplate for your HTML code for a slideshow. Please, refer to *index.html* sample as a complete example.

### DOM structure for a slideshow ###
Here is the reference implementation for the slideshow container and for structured content. Follow these rules during embedding of your slideshow in your HTML code of the page.
Look on the reference numbers inside comments "<!-- [ ] -->" and on the related footnotes below the sample for details.

    <!-- [1] -->
    <ul data-slideshow="true">

        <!-- [2] -->
        <li class="slide">

            <!-- [3] -->
            <section class="image">
                <!-- [3a] -->
                <img src="/your-image-source" data-slide-id="an-article-1"/>
                <!-- [3b] -->
                <div class="logo">Optional logo</div>
            </section>

            <!-- [4] -->
            <article class="story">
                <!-- [4a] -->
                <header>
                    <h1>Caption</h1>
                </header>

                <!-- [4b] -->
                <section class="summary">
                    Sample text 
                </section>

                <!-- [4c] -->
                <section class="details">
                    <ul class="collapsible-group">
                        <li>
                            <h2 class="collapsible-group-title">Title 1</h2>
                            <div>
                                Some text.
                            </div>
                        </li>
                        <li>
                            <h2 class="collapsible-group-title">Title 2</h2>
                            <div>
                                Some text.
                            </div>
                        </li>
                        <li>
                            <h2 class="collapsible-group-title">Title 3</h2>
                            <div>
                                Some text.
                            </div>
                        </li>
                    </ul>
                </section>

                <!-- [4d] -->
                <section class="tip">
                    Tip: a sample.
                </section>

            </article>

            <!-- [5] -->
            <section class="author">
                <address>Author: the <a href="#">Nature</a></address>
            </section>

            <!-- [6] -->
            <section class="toggle-view-prompt">
                Learn how to style &amp; more
            </section>

        </li>

        ...

        <!-- [7] -->

        ...

    </ul>

**[1]** Container for the entire slideshow. Slideshow will be applied to each unordered list with an attribute **data-slideshow**.

**[2]** Container for a single slide (class="slide").

**[3]** An image logical section (a section tag is recommended; class="image").

**[3a]** Image tag (Contains a main image for slide). Optional attribute - **data-slide-id** (allows to define a "decorated" links for this slide, follow rules for charset allowed in URL).

**[3b]** Optional "floating" logo for image (appears for active slide as a float box over this image, with semi-transparent background). 

**[4]** Logical section for story, related to image above.

**[4a]** Header (logical entity), contains header with level H1 and below.

**[4b]** Optional logical section for "summary" of the story (static content at the top of the story).

**[4c]** The main body of the story (contains a collapsible group of sub-articles). The collapsible group itself has a following structure (closing tags are not shown here):

    <ul class="collapsible-group">
        <li>
            <h* class="collapsible-group-title">
            <div>   

Where header (h2 or below) contains a caption text for the "title" of collapsible item, and DIV contains the collapsible content.

**[4d]** Optional logical section for a "footer" (static content at the bottom of the story). 

**[5]** Placeholder for attribution, author credentials, etc.

**[6]** Optional label for the switch button (switches image/story view for slide in the narrow screen mode).

**[7]** Other slides...

## Desktop and mobile ##
Slideshow has a different appearance for mobile devices (tablet, smartphone) and for notebook / desktop. Main differences for mobiles are:

1. Screen splitting (active slide occupies more space of the visible region)
2. A slide story is hidden by default. You can switch between 2 modes (image or story) by a button at the bottom of each slide
3. Inside article, each sub-article of a collapsible group has independent behavior (state of another child remains the same when you opening or closing a new child). The whole story has its common scrollbar. In the desktop version, each sub-article can has its own scrollbar. 
4. Background color and opacity.

## Notes for designers ##

Plugin automatically wraps some user-defined elements into additional containers (div), for a better control over appearance and dimensions (such appoach allows to avoid complexity of HTML code of the slideshow content). 
Please, note this behavior, unless you want to override some default CSS rules or to define your own (you can see a resulting structure of the DOM in example, using the developer console of the browser).

