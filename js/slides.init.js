 $(document).ready(function(){

 	var
 		slider, 
 		lockHashListener = 0,	
 		allowHashUpdate = false,
 		isMaximized = false,
 		sliderOptions = {
			item: 2,
			autoWidth: false,
			slideMargin: 0,

			addClass: '',
			mode: "slide",
			useCSS: true,
			cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
			easing: 'linear', //'for jquery animation',////

			speed: 400, //ms'
			auto: false,
			loop: true,
			slideEndAnimation: true,
			pause: 2000,

			keyPress: false,
			controls: true,
			prevHtml: '<i class="fa fa-angle-left slideset-ctrl"></i>',
			nextHtml: '<i class="fa fa-angle-right slideset-ctrl"></i>',

			rtl:false,
			adaptiveHeight:false,

			vertical:false,
			verticalHeight:500,
			vThumbWidth:100,

			thumbItem:10,
			pager: true,
			gallery: false,
			galleryMargin: 5,
			thumbMargin: 5,
			currentPagerPosition: 'middle',

			enableTouch:true,
			enableDrag:false,
			swipeThreshold: 40,

			responsive : [],

			onBeforeStart: function (el) {},
			onBeforeSlide: function (el) {$(document).triggerHandler('nextslide')},
			onAfterSlide: function (el) {
				if (allowHashUpdate) {
					allowHashUpdate = false;
					updateWindowHash (el);
				}
			},
			onBeforeNextSlide: function (el) {allowHashUpdate = true;},
			onBeforePrevSlide: function (el) {allowHashUpdate = true;},

			onAfterResize: function (el) {setTimeout(function () {
				if (!isMaximized && window.innerWidth == screen.width) {
					isMaximized = true;
					// slider.refresh();
					slider.destroy();
					slider.attr('style', '');
					slider = $('ul[data-slideshow]')
						.lightSlider(sliderOptions);

				} else {
					isMaximized = false;
					goToSlideByUrlHash(slider);
				}
			}, 100)},

			onSliderLoad: function (el) {
				_trace_('onSliderLoad');
				isMaximized = window.innerWidth == screen.width;
				$(window).triggerHandler('hashchange');					
				$(window).triggerHandler('aquireupdate.slide');				
			},

		};

 	function unlockHashListener () {
 		lockHashListener--;
 	}

 	// decode url if bookmark used
 	function decodeUrlHash () {
 		var hash = window.location.hash.substr(1);
 		return (hash.length>0) ? hash : false;
 	}


	function updateWindowHash ($slideshow) {
		var el = $slideshow,
			dataSlideId = $(el).find('*.slide.active').attr('data-slide-id');
		_trace_('updateWindowHash, el: ', $slideshow);
		_trace_('active slide: ', dataSlideId);
		lockHashListener++;
		setTimeout(unlockHashListener, 100);
		window.location.hash = '#'+dataSlideId;

	}

	function goToSlideByUrlHash () {
		var 
			hash = decodeUrlHash();
		_trace_('going to slide by hash', hash);
		// do nothing if hash changed from script:
		if (lockHashListener) 
			return;
		if (hash) {
			goToSlideById(slider, hash);
		}
		else {
			// No hash, go to the first slide:
			slider.goToSlide(1);
		}
		_trace_('active id: ', hash);
		_trace_('active slide: ', slider.find('*.slide.active'));
	}

	function goToSlideById (slideShow, id) {
		var 
			slide = $('*.slide[data-slide-id=\"'+id+'\"]'),
			slideOrder = slide.attr('data-slide-order');

		if (slideOrder) {
			slideOrder = parseInt(slideOrder);
			slideShow.goToSlide(slideOrder);
		} else {
			// not found, ignore
			_trace_('Cannot find slide with id: '+id);
		}
	};

 	slider = $('ul[data-slideshow]')
 		.formatSlides()
		.lightSlider(sliderOptions)
		// "disable" children of "inactive" slideSet:
		.on('click', '.slide', function (event) {
			if ($(this).hasClass('inactive')) {
				event.stopPropagation();
				event.preventDefault();
				return false;
			}
		});

	$(window).on('hashchange onhashchange dimensionsready.slide', goToSlideByUrlHash);

});