// slides.render.js
var _trace_ = function () {};

(function ($) {

	// redefine standard "height" functions in jQuery

	function fromPx (s) {
		var value = (typeof s == 'string') ? s.replace('px', '') : s;
		try{
			return Math.abs(Math.floor(parseFloat(value)));
		} catch (e) {
			return null;
		}
	}

	$.fn._getHeight = function () {
		var el = $(this), height=0;
		// return fromPx(el.css('height')) || fromPx(el.css('line-height'));
		if (el.length>1) {
			el.each(function () {
				height = Math.max(height, fromPx($(this).css('height')));
			});
		} else {
			h = fromPx(el.css('height'));
		}
		return h;
	}

	$.fn._getInnerHeight = function () {
		var el = $(this);
		return el._getHeight() - fromPx(el.css('padding-top')) - fromPx(el.css('padding-bottom'));
	}

	$.fn._getOuterHeight = function () {
		var el = $(this);
		return el._getHeight() + fromPx(el.css('margin-top')) + fromPx(el.css('margin-bottom'));
	}

	function IdProcessor (prefix, attrName) {
		// Preserve a manually assigned Id or create and apply a new one.

		var lastId = 0;
		attrName = attrName || 'data-slide-id';
		prefix = prefix ? prefix.toString() : '';

		this.nextId = function () {
			return prefix + (++lastId);
		}

		this.IdOf = function ($element, defaultId) {
			var
				manId = $element.attr(attrName),
				id;

			// 1. do nothing if ID already exists:
			if (manId) return manId;

			// 2. try to use defaultId, if any:
			if (defaultId === null || typeof defaultId == 'undefined') {
				id = this.nextId();
				// ensure that ID is unique:
				// while ($('#'+id).length>0) {
				// 	id = this.nextId();
				// }

			} else {
				// use default if any:
				id = defaultId;
			}

			// 3. apply ID value to attribute:
			$element.attr(attrName, id);

			return id;			
		}
	}

	function createIdFromImgAttrs ($img) {
		// return ID of IMG, or filename portion without path and extension, otherwise null;
		var id = $img.attr('data-slide-id');
		if (id) return id;
		var src = $img.attr('src');
		if (src) return src.split('/').pop().split('.')[0];
		return null;
	}

	function isMobile () {
		return $('body').data('scr-mode') <= 2;
	}

	function updateDimensions () {
		_trace_('updating dimensions; active slide:', $('ul[data-slideshow]').find('*.slide.active').attr('data-slide-id'));
		// ***
		// detect screen is changed:
		var sMode = 
			$('#screen-mode-tn:visible').length && 1 ||
			$('#screen-mode-xs:visible').length && 2 ||
			$('#screen-mode-sm:visible').length && 3 ||
			$('#screen-mode-md:visible').length && 4 ||
			$('#screen-mode-lg:visible').length && 5 || 0;

		if ($('body').data('scr-mode') == sMode) { return };

		$('body').data('scr-mode', sMode);

		_trace_('updating..........');

		$('*.img-slideshow').each(function () {
			var 
				slideSet = $(this),
				slides = slideSet.find('*.slide'),
				contents = slides.find('*.slide-content'),
				parentHeight = slideSet._getInnerHeight(),
				hSlideNumbering = 0,
				hAuthor = 0,
				hHeader = 0,
				hSummary = 0,
				hTip = 0,
				hBtnDetails = 0,
				childClientHeight,
				collapsibleGroupHeight,
				_max = Math.max;

			_trace_(slideSet, parentHeight);

			slides.each(function () {
				var 
					slide = $(this),
					contentSidebar = slide.find('*.article-panel');

				hSlideNumbering = _max(hSlideNumbering, slide.find('*.slide-numbering')._getOuterHeight(true) || 0),
				hAuthor = _max(hAuthor, slide.find('section.author>div')._getOuterHeight(true) || 0),
				hHeader = _max(hHeader, contentSidebar.find('header>*')._getOuterHeight(true) || 0),
				hSummary = _max(hSummary, contentSidebar.find('*.summary>*')._getOuterHeight(true) || 0),
				hTip = _max(hTip, contentSidebar.find('*.tip>*')._getOuterHeight(true) || 0),
				hBtnDetails = _max(hBtnDetails, contentSidebar.find('*.btn-details')._getOuterHeight(true) || 0);

			});

			childClientHeight = parentHeight - hSlideNumbering - hAuthor; // <- remove 2x3 border-radius
			collapsibleGroupHeight = childClientHeight - hHeader - hSummary - hTip  - (isMobile() ? hBtnDetails : 0) - (isMobile()? 20 : 6);

			contents.find('*.image *.image-panel')
				.css({
					'height': childClientHeight,
					'min-height': childClientHeight
				});

			contents.find('*.story *.article-panel')
				.css({
					'height': childClientHeight,
					'min-height': childClientHeight,
					'margin-top': (isMobile()) ? '-'+childClientHeight+'px' : ''	
				});

			contents.find('*.details *.collapsible-group')
				.css({
					'height': collapsibleGroupHeight,
					'min-height': collapsibleGroupHeight
				});

			// contents.find('*.story>div.col').css('margin-top', (isMobile()) ? '-'+childClientHeight+'px' : '')

			$(window).triggerHandler('dimensionsready.slide');

		});
	}

	function deferredUpdateDimensions () {
		updateDimensions();
		// setTimeout(updateDimensions, 1100); // <-- in lightslider, updates is fired after 1 second only!
	}

	var 
		slideSetIdProcessor = new IdProcessor('slideset-', 'id'),
		// this processor necessary to create deep bookmark: 
		slideIdProcessor = new IdProcessor('', 'data-slide-id');

	$.fn.formatSlides = function (jsonData) {

		function processSlideset() {
			var container = $(this);
			var slidesetID = slideSetIdProcessor.IdOf(container);
			var slides = container.find('*.slide');
			var count = slides.length;
			var $img;
			var sOrder = 0;
			var toggleViewPrompt;

			// apply metaclass to the root element of slideset:
			container.addClass('img-slideshow');

			// Apply IDs for deep linking
			slides.each(function (index, item) {
				var 
					$slide = $(item)
						.attr('data-slideshow-id', slidesetID) // <- make weak reference to the parent slideshow
						.attr('data-slide-order', (++sOrder)), // <- store slide order
					id = slideIdProcessor.IdOf($slide, createIdFromImgAttrs($slide.find('*.image img'))),
					anchor = $('<a/>')
						.attr('href', '#'+id)
						// .attr('name', id)
						.attr('rel', 'bookmark')
						.attr('title', 'Link for bookmark')
						// .html('<i class="fa fa-link"></i>'),
					$header = $slide.find('header>h1, header>h2, header>h3');

				// Create link:		
				$header.prepend('<span>&nbsp;</span>').append(anchor);
			});

			// Make wrapping 
			slides.wrapInner('<div class="grid-section group slide-content"></div>');

			$('<div class="grid-section group slide-numbering"><div class="col span_2_of_2">'
				+ '<span class="slide-index"></span> of '
				+ count
				+'</div></div>').prependTo(slides);

			container.find('*.slide-index').each(function (index) {
				$(this).html(index+1);
			});

			slides.find('*.image').wrapInner(
				'<div class="col span_1_of_2 image-panel"></div>'
				);
				// .children('div').find('img').after(
				// 	$('<div/>').addClass('btn-details TEST').html('Read more...&nbsp;<i class="fa fa-caret-up"></i>')
				// );


			// slides.find('*.story *.details > ul')
			// 	.children('li').addClass('collapsible-group');

			slides.find('*.story').wrapInner(
				'<div class="col span_1_of_2 article-panel"></div>'
				)
				.find('*.collapsible-group *.collapsible-group-title')
				.attr('data-slideset-id', slidesetID)
				// .append('<div class="ico"></div>');
				.append('<i class="fa fa-caret-up"></i><i class="fa fa-caret-down"></i>');

			// ***
			// slides.find('*.details').wrapInner(
			// 	'<div style="display:block;position:relative;width:100%;height:100%;"></div>'
			// 	);

			slides.each(function () {
				var slide = $(this),
					promptEl = slide.find('section.toggle-view-prompt'),
					toggleViewPrompt = promptEl.length > 0 ? promptEl.html() : 'Read more...';
				slide
					.find('*.image *.col, *.story *.col')
					.append(
							$('<div/>').addClass('btn-details').html('<span>'+toggleViewPrompt + '</span>&nbsp;<i class="fa fa-caret-up"></i>')
						);
				if (promptEl.length > 0) promptEl.remove();
			});

			// // old version was:
			// slides.find('*.image *.col, *.story *.col')
			// 	.append(
			// 			$('<div/>').addClass('btn-details').html('Read more...&nbsp;<i class="fa fa-caret-up"></i>')
			// 		);

			slides.find('*.author')
				.wrapInner(
					// '<div class="col span_1_of_2"></div>'
					'<div class="col span_1_of_2"></div>'
				);

			slides.find('*.tip').wrapInner(
				'<div></div>'
				);

			slides.find('*.summary').wrapInner(
				'<div></div>'
				);

			slides.find('*.header').wrapInner(
				'<div></div>'
				);

		}


		return this.each ( processSlideset );

	}

/*
#screen-mode-tn,
#screen-mode-xs,
#screen-mode-sm,
#screen-mode-md,
#screen-mode-lg {display: none}

*/

	$('body').prepend(
			'<div id="screen-mode-tn"></div>' + 
			'<div id="screen-mode-xs"></div>' +
			'<div id="screen-mode-sm"></div>' +
			'<div id="screen-mode-md"></div>' +
			'<div id="screen-mode-lg"></div>' 
		);

	// install handler to update dimensions for mobile/desktop
	// $(window).on('resize', function () {});

	// trigger this event when slider loaded or when necessary:
	$(window).on('resize aquireupdate.slide', deferredUpdateDimensions);

	// install handler for toggle group
	$(document).on('click', '*.collapsible-group-title', function (event) {
		var title = $(event.target),
			collapsibleGroup = title.closest('*.collapsible-group'),
			clientHeight = collapsibleGroup._getInnerHeight();

		// clear previously applied height to the collapsible divs:
		collapsibleGroup.children('li').children('div').css('height', '');

		if (isMobile()) {
			// mobile display
			title.toggleClass('opened');

		} else {
			// normal display
			var wasOpened = title.hasClass('opened');
			collapsibleGroup.find('*.opened').removeClass('opened');
			if (wasOpened) {
				title.next('div').height('0px'); 
				return;
			}
			collapsibleGroup.children('li').children('h1,h2,h3,h4,h5').each(function () {
				clientHeight -= $(this)._getOuterHeight(true); // <-- compensate 2x3px border-radius
			});
			title.next('div').height(Math.floor(clientHeight)); 
			title.addClass('opened');
		}
	});

	$(document).on('click', '*.image *.btn-details', function (event) {
		var $button = $(event.target);
		$button.closest('*.image').toggleClass('on-popup').next('*.story').toggleClass('on-popup');
	})
	.on('click', '*.story *.btn-details', function (event) {
		$('*.on-popup').removeClass('on-popup');
	})
	.on('nextslide', function () {
		$('*.on-popup').removeClass('on-popup');
	});

	
})(jQuery);


